Name:			Simple-HTPd
Version:		v1.1.0
Release:        1%{?dist}
Summary:		A simple time synchronization over HTP (HTTP Time Protocol)

License:		GPL
URL:			https://gitlab.com/everest-code/simple-htpd
Source0:		https://gitlab.com/everest-code/simple-htpd/-/archive/%{version}/simple-htpd-%{version}.tar.gz

Requires:		systemd
Requires:		python3
Requires:		python3-toml
Requires:		python3-requests

BuildRoot:		~/rpmbuild/

%description
A simple time synchronization over HTP (HTTP Time Protocol).

%prep
RPM_BUILD_SOURCE=~/rpmbuild/SOURCES/%{name}
echo "BUILDROOT = ${RPM_BUILD_ROOT}"
echo "BUILDSOURCE = ${RPM_BUILD_SOURCE}"

%{__mkdir_p} $RPM_BUILD_ROOT/usr/local/lib/simple_htpd
%{__mkdir_p} $RPM_BUILD_ROOT/etc/simple_htpd/
%{__mkdir_p} $RPM_BUILD_ROOT/usr/lib/systemd/system/

%{__cp} -rv $RPM_BUILD_SOURCE/config/simple_htpd.toml $RPM_BUILD_ROOT/etc/simple_htpd/.
%{__cp} -rv $RPM_BUILD_SOURCE/config/simple_htpd.service $RPM_BUILD_ROOT/usr/lib/systemd/system/.
%{__cp} -rv $RPM_BUILD_SOURCE/{__main__,config,errors,htp,utils}.py $RPM_BUILD_ROOT/usr/local/lib/simple_htpd
%{__cp} -rv $RPM_BUILD_SOURCE/run.sh $RPM_BUILD_ROOT/usr/local/lib/simple_htpd

%post
ln -s /usr/local/lib/simple_htpd/run.sh /usr/bin/simple_htpd
systemctl daemon-reload
systemctl enable --now simple_htpd.service

%preun
systemctl stop simple_htpd.service
systemctl disable simple_htpd.service

%postun
rm -rdfv /usr/bin/simple_htpd
systemctl daemon-reload

%files
%attr(0644, root, root) /usr/local/lib/simple_htpd/{config,errors,htp,utils}.py
%attr(0755, root, root) /usr/local/lib/simple_htpd/{__main__.py,run.sh}
%attr(0644, root, root) /etc/simple_htpd/simple_htpd.toml
%attr(0644, root, root) /usr/lib/systemd/system/simple_htpd.service

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rdfv $RPM_BUILD_ROOT
