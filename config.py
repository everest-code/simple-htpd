from toml import loads as toml_parse
from typing import Tuple, Optional

from utils import AttrDict
from errors import Error


def get_conf(filename: str) -> Tuple[Optional[AttrDict], Optional[Error]]:
    try:
        with open(filename, "r") as file:
            data = file.read()
    except Exception as err:
        return None, Error.parse_exception(err)

    return AttrDict(toml_parse(data)), None
