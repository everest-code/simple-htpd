from __future__ import annotations
from typing import Any, List
from random import choice as get_random


class AttrDict(dict):
    def __setattr__(self, key: str, value: Any):
        self[key] = value

    def __getattr__(self, key: str) -> Any:
        if key not in self:
            raise AttributeError(f'The attribute "{key}" does not exist')
        else:
            return self[key]

    def __delattr__(self, key: str) -> Any:
        if key not in self:
            raise AttributeError(f'The attribute "{key}" does not exist')
        else:
            del self[key]


class OriginList(list):
    def __add__(self, origin: str) -> OriginList:
        new = self[:]
        new.append(origin)
        return OriginList(new)

    def __sub__(self, origin: str) -> OriginList:
        new = self[:]
        new.remove(origin)
        return OriginList(new)
