from requests.api import get as fetch
from datetime import datetime
from dateutil.parser import parse as parse_time
from typing import Tuple, Optional
from errors import Error
from os import system


def get_date(origin: str) -> Tuple[Optional[datetime], Optional[Error]]:
    start = datetime.now()
    try:
        req = fetch(origin)
    except Exception as err:
        return None, Error.parse_exception(err)
    diff = datetime.now() - start
    del start

    if "Date" not in req.headers:
        return None, Error("KeyError", 'The key "Date" not found in headers')

    return parse_time(req.headers["Date"]) + diff, None


def set_date(new_date: datetime):
    print(f'Setting date "{new_date}"')
    system(f'date -s "{new_date}"')
