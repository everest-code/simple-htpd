#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- author: sgt911 -*-
from htp import get_date, set_date
from config import get_conf
from utils import get_random, OriginList
from errors import raise_error, warning_error, Error
from time import sleep

from typing import Tuple, Optional
from datetime import datetime


def fetch_date(
    origins: OriginList, max_tries: int = 0
) -> Tuple[Optional[datetime], Optional[Error]]:
    if len(origins) == 0:
        return Error("RuntimeError", "The origins list are empty")

    origin = get_random(origins)
    print(f'Using origin "{origin}"')
    date, err = get_date(origin)

    if err is not None and max_tries > 0:
        return fetch_date(origins - origin, max_tries - 1)

    return date, err


def main(config_file: str):
    config, err = get_conf(config_file)
    if err is not None:
        return raise_error(err, 2)

    origins = OriginList(config.origins)
    awaitng_time = config.refresh_period * 60

    while True:
        date, err = fetch_date(origins, config.max_tries)

        if err is not None:
            warning_error(err)
            sleep(awaitng_time * 2)
            continue

        if not config.dry_run:
            set_date(date)
        else:
            print(f"New date = {date}")

        sleep(awaitng_time)


if __name__ == "__main__":
    from sys import argv

    main(*argv[1:])
