from __future__ import annotations
from typing import Any, Optional
from sys import exit


class Error:
    def __init__(self, err_type: str, msg: Any):
        self.error = err_type
        self.message = msg

    @staticmethod
    def parse_exception(ex: Exception) -> Error:
        return Error(ex.__class__.__name__, ex.args)


def raise_error(err: Optional[Error], exit_status: int):
    if err is not None:
        print("Error -> %s(%r)" % (err.error, err.message))
    exit(exit_status)


def warning_error(err: Optional[Error]):
    if err is not None:
        print("Warning -> %s(%r)" % (err.error, err.message))
